
import pandas as pd
import numpy as np
import datetime
import string
from elasticsearch import Elasticsearch, helpers
from elasticsearch.helpers import scan
import argparse


# access to elastic search server in chicago
es = Elasticsearch([{'host':'atlas-kibana.mwt2.org', 'port':9200}],timeout=60)

limit = 400000        # maximum number of records to save


# read the input parameters
parser = argparse.ArgumentParser(description='command line client for data extraction')
parser.add_argument('--taskid', type=int ,help='the taskid for the query', required=True)
parser.add_argument('--cached', help='if true, data requested by query is cached; no need to contact Conditions DB')
parser.add_argument('--disconn',help='if true, Frontier server got disconnected from Conditions DB server')
parser.add_argument('--delay', type=int ,help='by default 15 minutes',default=15)
parser.add_argument('--date', type=datetime.datetime ,help='(YYYY-MM-DDThh:mm:ss)present all queries with timestamp= date - delay')
parser.add_argument('--From', type=datetime.datetime ,help='all queries from this date (YYYY-MM-DDThh:mm:ss)')
parser.add_argument('--to', type=datetime.datetime ,help='all queries started before this date (YYYY-MM-DDThh:mm:ss)')
argument =parser.parse_args()



my_query = {
    "size": 0,
    "_source": ["pandaid", "cached", "sqlquery", "disconn", "querytime", "queryiov", "fsize", "@timestamp"],
    "query": {

        "bool": {
            "must": [
                {"term": {"taskid": argument.taskid}},
                {"bool": {
                    "should": [

                    ],
                }}

            ],
        }

    }
}



# depending on the input value, the data extract from the es server will be filtered
if argument.cached != None:
    my_query['query']['bool']['must'][1]['bool']['should'].append({'term': {'cached': argument.cached}})
    #: {'cached' : cached}})
if argument.disconn != None:
    my_query['query']['bool']['must'][1]['bool']['should'].append({'term': {'disconn': argument.disconn}})
if argument.date != None :
    query_timestamp = argument.date - datetime.timedelta(minutes=argument.delay)
    my_query['query']['bool']['must'].append({ 'term' : {'@timestamp': query_timestamp}})
if argument.From != None and argument.To != None :
    my_query['query']['bool']['must'].append({ 'range' : {'@timestamp': { "gte": argument.To, "lte": argument.From }}})



print(my_query['query']['bool']['must'][1]['bool']['should'])


# extract data from es server
res = helpers.scan(es, query=my_query, index='frontier-new', scroll='5m', timeout="5m", size=1000)
data = []
counter = 0
for r in res:
    counter+=1
    if counter>limit: break
    if not counter%5000: print ("loaded:", counter)
    data.append(r['_source']) # put the results in data list


# the results are stored in panda dataframe format
print ('finished loading. total of',counter, 'rows.')
df = pd.DataFrame(data)
df

# saving data as compress hdf5 file
store = pd.HDFStore('frontier.h5', complevel=2)
store.append('tid_'+str(argument.taskid), df)









