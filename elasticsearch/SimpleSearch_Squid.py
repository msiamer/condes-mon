
import pandas as pd
import numpy as np
import datetime
import string
from elasticsearch import Elasticsearch, helpers
import argparse
from elasticsearch.helpers import scan

es = Elasticsearch([{'host':'atlas-es-prod.roma1.infn.it', 'port':9200}],timeout=60)

limit = 400000        # maximum number of records to save


# read the input parameters
parser = argparse.ArgumentParser(description='command line client for data extraction')
parser.add_argument('--taskid', type=int ,help='the taskid for the query', required=True)
parser.add_argument('--request_status', help='define the request status : TCP_HIT, TCP_MISS, TCP_MEM_HIT, TCP_REFRESH_MODIFIED, TCP_REFRESH_UNMODIFIED')
parser.add_argument('--response_time',help='the time needed to send response for the query')

argument =parser.parse_args()



my_query={
    "size": 0,
    "_source": ["panda_id","squid_request_status","squid_query","squid_response_time","reply_size","@timestamp"],
    "query": {

        "bool": {
            "must": [
                  {
                      "term": {"task_id":argument.taskid},
                      'term': {'tags': 'squid_in2p3'},
                  },

                  { "bool" : {
                      "should": [

                      ],
                   }}

                   ],
        }

        }
    }



# depending on the input value, the data extract from the es server will be filtered
if argument.request_status != None:
    my_query['query']['bool']['must'][1]['bool']['should'].append({"match": {
        "squid_request_status": {
            "query": argument.request_status,

        }
    }})
if argument.response_time != None:
    my_query['query']['bool']['must'].append({"match": {
        "squid_response_time": {
            "query": argument.response_time, }}
    })





# extract data from es server
res = helpers.scan(es, query=my_query, index='squid-*', scroll='5m', timeout="5m", size=1000)
data = []
counter = 0
for r in res:
    counter+=1
    if counter>limit: break
    if not counter%5000: print ("loaded:", counter)
    data.append(r['_source'])

print ('finished loading. total of',counter, 'rows.')
df = pd.DataFrame(data)
df

# saving data as compress hdf5 file
store = pd.HDFStore('squid.h5', complevel=2)
store.append('tid_'+str(argument.taskid), df)



