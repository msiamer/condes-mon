# CLI for ES conditions monitoring analytics


## Author : Si Amer Millissa

### Overview 
CLI data extraction client in python for selectively extracting and aggregating ES-resident data and producing output in a panda dataframe format.

### description 
A current priority for ATLAS is to analyze and understand the behaviour of our distributed conditions database system, particularly under conditions that stress its scalability. 
The scalability of the system depends on effective caching of database interactions in the squid web proxy hierarchy between the client and Frontier services (Launchpads). 
Data sources include the squid hierarchy and the Frontier/Crest services. 
The practical objective of the project is to extract and parse the conditions access queries from ElasticSearch DB, make comparisons between cached and not cached queries, rejected and disconnected queries to understand the caching behavior and performance of the system. 
This project allows the user to extract data from both Chicago and Rome servers where frontier and squid web proxy hierarchy logs are stored. 
It's commande line client where you can specify filters to get a specific data from elastic search servers. The different input parameters are mentionned below : 
  - --taskid : atlas-es-prod.roma1.infn.it
  
for Frontier server (Chicago):
  - --cached: if true, data requested by query is cached; no need to contact Conditions DB
  - --disconn: if true, Frontier server got disconnected from Conditions DB server'
  - --date : represents the start date of the query execution 
  - --delay: by default 15 minutes, we use it to get queries with timestamp = date - delay
  - --From --To: queries ran during this period
  
for Squid server (Rome):
  - --request_status:  define the request status : TCP_HIT, TCP_MISS, TCP_MEM_HIT, TCP_REFRESH_MODIFIED, TCP_REFRESH_UNMODIFIED
  - --response_time: the time needed to send response for the query 
  
### Prerequisites
You need to install:
    python3
    
### Setup 
configure your Lxplus account by creating a virtual environment for Python 3.6.3:
```
  scl enable rh-python36 bash
  virtualenv /your path/py36
  
```
For every session, you need to activate this python:
```
  source /your path/py36/bin/activate
  
```

For the first configuration, elasticsearch should be installed : 
```
  pip3.6 install elasticsearch
  pip3.6 install panda
  pip3.6 install tables
  
```

### Running 
To run the Frontier script :
```
   python SimpleSearch.py --taskid 14375483 --option value
```
To run the Squid script :
```
   python SimpleSearch_Squid.py --taskid 14375483 --option value
```


